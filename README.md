# USBC-to-RS485

# Function: 
Small Converter from USB to RS485. USBC 2.0 connection  and MC 1,5/ 4-GF-3,5 on the other side. Main goal was to keep it small. 

# Status: 

### First Revision 

Works unstable, without modification. With the following two modifications the device works as intended. 

To work stable its needed to short the Test pin to GND . This can be done with a small solder blob, there is a GND pin directly next to the TEST Pin. (Pin 25&26 )

Unwanted behaviour in intended use: The transmitted Data is echoed back to the receiver line. To prevent it, the FTDI Programming utility can be used to configure the CBUS3 function to TXDEN.

The Utility cam be downloaded [here](https://ftdichip.com/utilities/), the exact name is : FT_PROG 3.12.54.665 - EEPROM Programming Utility

![image](https://git.rwth-aachen.de/acs/public/hardware/rs485-to-usb/-/raw/main/Flasch_Util.png?ref_type=heads)

The image highlights where all important things can be found to programm the FTDI Chip correctly. Check, that C3 is set to TXDEN. 

However, the utility is only available for windows, there is a third party [Linux Tool](https://github.com/richardeoin/ftx-prog), which is not tested by me. 

##### Linux Programming: 

The following seems to be a suitable programming utility

http://developer.intra2net.com/git/?p=libftdi;a=blob;f=README.build



## License
CERN-OSH-P , find full text in the license file
